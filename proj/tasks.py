from .celery import app
from random import randint
import time


@app.task
def mul(r, x, y):
    return 'Mull res: ' + r + str(x * y)


@app.task
def xsum(numbers):
    return sum(numbers)


@app.task
def add(n, x, y):
    delay = randint(0, 5)
    time.sleep(delay)
    return 'Line #: {}, result: {}, custom dealy: {}, Processed by service "Second", finished: {} | '. \
        format(n, x + y, delay, time.time())
