## INSTALL
    - `pip install -r requirements.txt`
 
## RUN
    - Run a worker to consume the tasks `celery -A proj worker -Q some_task_add,some_task_xsum,some_task_mul -l info`